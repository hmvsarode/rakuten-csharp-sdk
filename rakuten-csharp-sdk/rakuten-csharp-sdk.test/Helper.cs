﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RakutenWebService
{
    internal class Helper
    {
        /// <summary>アプリケーションID</summary>
        internal const string APPLICATION_ID = "REPLACE YOUR APPLICATION ID";

        /// <summary>文字列二つからKeyValuePairを作成する</summary>
        /// <param name="key">鍵</param>
        /// <param name="value">値</param>
        /// <returns>KeyValuePair</returns>
        internal static KeyValuePair<string, string> entry(string key, string value)
        {
            return new KeyValuePair<string, string>(key, value);
        }

        /// <summary>可変長でKVPを受けて辞書を作成する</summary>
        /// <param name="parameters">KeyValuePair群</param>
        /// <returns>辞書</returns>
        internal static IDictionary<string, string> parameters(params KeyValuePair<string, string>[] parameters)
        {
            var dictionary = new Dictionary<string, string>();
            parameters.ToList().ForEach(kvp => dictionary.Add(kvp.Key, kvp.Value));
            return dictionary;
        }
    }
}
