﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RakutenWebService
{
    [TestClass]
    public class ReadMe
    {
        [TestMethod]
        public void HelperクラスのAPPLICATION_IDを置き換えてください()
        {
            Helper.APPLICATION_ID.IsNot("REPLACE YOUR APPLICATION ID", "HelperクラスのAPPLICATION_IDを貴方のIDと置き換えてください");
        }
    }
}
