﻿using Codeplex.Data;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RakutenWebService
{
    /// <summary>楽天タグ検索APIの振舞い</summary>
    [TestClass]
    public class IchibaTagSearchBehavior
    {
        #region Fields
        /// <summary>楽天タグ検索API</summary>
        private AbstractSearch searcher;
        #endregion

        #region Properties
        /// <summary>
        /// <para>テストコンテキスト</para>
        /// <para>パラメトライズテストに必要</para>
        /// </summary>
        public TestContext TestContext { get; set; }
        #endregion

        #region Arrangements
        /// <summary>テストメソッド実行前のセットアップ</summary>
        [TestInitialize]
        public void SetUp()
        {
            searcher = new IchibaTagSearch(Helper.APPLICATION_ID);
        }
        #endregion

        #region Behavior
        #region case of unregistered application id.
        [TestMethod]
        public void 登録されていないアプリケーションIDで検索すると400エラーを返すべき()
        {
            var ex = AssertEx.Throws<HttpException>(() => unregisteredSercher().Search(someTag()));
            ex.StatusCode.Is(HttpStatusCode.BadRequest);
        }
        #endregion

        #region case of bad parameters.
        [TestMethod]
        public void 存在しないジャンルIDで検索すると400エラーを返すべき()
        {
            var ex = AssertEx.Throws<HttpException>(() => searcher.Search(unexistTag()));
            ex.StatusCode.Is(HttpStatusCode.BadRequest);
        }
        #endregion

        #region case of some search results.
        [TestMethod]
        public void 存在するタグIDならタググループを取得できるべき()
        {       
            var json = DynamicJson.Parse(searcher.Search(someTag()));
            ((bool)json.tagGroups()).Is(true);
        }
        #endregion
        #endregion

        #region TestParameters
        private AbstractSearch unregisteredSercher()
        {
            return new IchibaTagSearch("UnregisterdId"); ;
        }

        private IDictionary<string, string> someTag()
        {
            return Helper.parameters(Helper.entry("tagId", "1000319"));
        }

        private IDictionary<string, string> unexistTag()
        {
            return Helper.parameters(Helper.entry("tagId", "unexistId"));
        }

        #endregion
    }
}