﻿using Codeplex.Data;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RakutenWebService
{
    /// <summary>楽天商品ランキングAPIの振舞い</summary>
    [TestClass]
    public class IchibaItemRankingBehavior
    {
        #region Fields
        /// <summary>楽天商品ランキングAPI</summary>
        private AbstractSearch searcher;
        #endregion

        #region Properties
        /// <summary>
        /// <para>テストコンテキスト</para>
        /// <para>パラメトライズテストに必要</para>
        /// </summary>
        public TestContext TestContext { get; set; }
        #endregion

        #region Arrangements
        /// <summary>テストメソッド実行前のセットアップ</summary>
        [TestInitialize]
        public void SetUp()
        {
            searcher = new IchibaItemRanking(Helper.APPLICATION_ID);
        }
        #endregion

        #region Behavior
        #region case of unregistered application id.
        [TestMethod]
        public void 登録されていないアプリケーションIDでJSON要求を送ると400エラーを返すべき()
        {
            var ex = AssertEx.Throws<HttpException>(() => unregisteredSercher().Search(expectedTotalRanking()));
            ex.StatusCode.Is(HttpStatusCode.BadRequest);
        }
        #endregion

        #region case of total ranking.
        [TestMethod]
        public void パラメータなしの場合総合ランキングが取得されるべき()
        {
            var json = searcher.Search(expectedTotalRanking());
            ((string)DynamicJson.Parse(json).title).Is("【楽天市場】ランキング市場 【総合】");
        }
        #endregion

        #region case of female ranking.
        [TestMethod]
        public void 性別女性を指定した場合総合女性ランキングが取得されるべき()
        {
            var json = searcher.Search(expectedFemaleRanking());
            ((string)DynamicJson.Parse(json).title).Is("【楽天市場】ランキング市場 【総合・女性】");
        }
        #endregion
        #endregion

        #region TestParameters
        private AbstractSearch unregisteredSercher()
        {
            return new IchibaItemRanking("UnregisterdId"); ;
        }

        private IDictionary<string, string> expectedTotalRanking()
        {
            return Helper.parameters();
        }
        private IDictionary<string, string> expectedFemaleRanking()
        {
            return Helper.parameters(Helper.entry("sex", "1"));
        }
        #endregion
    }
}