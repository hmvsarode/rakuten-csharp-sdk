﻿using System.Collections.Generic;

namespace RakutenWebService
{
    /// <summary>検索能力を与えるインターフェース</summary>
    public interface ISearchable
    {
        /// <summary>与えられた条件で検索を行う</summary>
        /// <param name="parameters">リクエストパラメータ</param>
        /// <exception cref="HttpException">HTTPエラー</exception>
        /// <returns>レスポンス</returns>
        string Search(IDictionary<string, string> parameters);
    }
}