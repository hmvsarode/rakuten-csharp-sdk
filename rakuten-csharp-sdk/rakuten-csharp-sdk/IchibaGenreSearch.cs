﻿using System.Collections.Generic;
using System.Linq;

namespace RakutenWebService
{
    /// <summary>楽天ジャンル検索API</summary>
    public sealed class IchibaGenreSearch : AbstractSearch
    {
        #region コンストラクタ(ファクトリを用意する)
        /// <summary>コンストラクタ</summary>
        /// <param name="applicationId">アプリケーションID</param>
        internal IchibaGenreSearch(string applicationId)
            : base(applicationId)
        {
            this.BaseUrl = BaseUrlConst.ICHIBA_GENRE;
        }
        #endregion

        #region 内部メソッド
        /// <summary>楽天ジャンル検索APIに無効なパラメータを除く</summary>
        /// <param name="parameters">リクエストパラメータ</param>
        /// <returns>有効なリクエストパラメータ</returns>
        protected override IDictionary<string, string> removeInvalidParameters(IDictionary<string, string> parameters)
        {
            return parameters.Where(p => AllowedParameters.IchibaGenreSearch().Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value);
        }
        #endregion
    }
}
