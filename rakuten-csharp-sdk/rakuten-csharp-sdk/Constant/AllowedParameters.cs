﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RakutenWebService
{
    /// <summary>API毎の許可されたパラメータを保持するクラス</summary>
    internal static class AllowedParameters
    {
        #region 内部メソッド
        /// <summary>楽天商品検索APIの許可されたパラメータを取得する</summary>
        /// <returns>許可されたパラメータ</returns>
        internal static IEnumerable<string> IchibaItemSearch()
        {
            return common().Concat(ichibaItemSearch());
        }

        /// <summary>楽天ジャンル検索APIの許可されたパラメータを取得する</summary>
        /// <returns>許可されたパラメータ</returns>
        internal static IEnumerable<string> IchibaGenreSearch()
        {
            return common().Concat(ichibaGenreSearch());
        }

        /// <summary>楽天タグ検索APIの許可されたパラメータを取得する</summary>
        /// <returns>許可されたパラメータ</returns>
        internal static IEnumerable<string> IchibaTagSearch()
        {
            return common().Concat(ichibaTagSearch());
        }

        /// <summary>楽天商品ランキングAPIの許可されたパラメータを取得する</summary>
        /// <returns>許可されたパラメータ</returns>
        internal static IEnumerable<string> IchibaItemRanking()
        {
            return common().Concat(ichibaItemRanking());
        }

        /// <summary>商品価格ナビ製品検索APIの許可されたパラメータを取得する</summary>
        /// <returns>許可されたパラメータ</returns>
        internal static IEnumerable<string> ProductSearch()
        {
            return common().Concat(productSearch());
        }
        #endregion

        #region プライベートメソッド
        /// <summary>共通パラメータ</summary>
        /// <returns>全サービス共通のパラメータ</returns>
        private static IEnumerable<string> common()
        {
            return new List<string>()            
            {
                "affiliateId",
                "format",
                "callback",
                "elements",
                "formatVersion",
            };
        }

        /// <summary>楽天商品検索APIの許可されたパラメータを取得する</summary>
        /// <returns>許可されたパラメータ</returns>
        private static IEnumerable<string> ichibaItemSearch()
        {
            return new List<string>()            
            {
                "keyword",
                "shopCode",
                "itemCode",
                "genreId",
                "tagId",
                "hits",
                "page",
                "sort",
                "minPrice",
                "maxPrice",
                "availability",
                "field",
                "carrier",
                "imageFlag",
                "orFlag",
                "NGKeyword",
                "purchaseType",
                "shipOverseasFlag",
                "shipOverseasArea",
                "asurakuFlag",
                "asurakuArea",
                "pointRateFlag",
                "pointRate",
                "postageFlag",
                "creditCardFlag",
                "giftFlag",
                "hasReviewFlag",
                "maxAffiliateRate",
                "minAffiliateRate",
                "hasMovieFlag",
                "pamphletFlag",
                "appointDeliveryDateFlag",
                "genreInformationFlag",
                "tagInformationFlag",             
            };
        }

        /// <summary>楽天ジャンル検索API固有のパラメータ</summary>
        /// <returns>許可されたパラメータ</returns>
        private static IEnumerable<string> ichibaGenreSearch()
        {
            return new List<string>()            
            {
                "genreId",
                "genrePath",
            };
        }

        /// <summary>楽天タグ検索API固有のパラメータ</summary>
        /// <returns>許可されたパラメータ</returns>
        private static IEnumerable<string> ichibaTagSearch()
        {
            return new List<string>()            
            {
                "tagId",
            };
        }

        /// <summary>楽天商品ランキングAPI固有のパラメータ</summary>
        /// <returns>許可されたパラメータ</returns>
        private static IEnumerable<string> ichibaItemRanking()
        {
            return new List<string>()            
            {
                "genreId",
                "age",
                "sex",
                "carrier",
                "page",
                "period",
            };
        }

        /// <summary>商品価格ナビ製品検索API固有のパラメータ</summary>
        /// <returns>許可されたパラメータ</returns>
        private static IEnumerable<string> productSearch()
        {
            return new List<string>()            
            {
                "keyword",
                "genreId",
                "productId",
                "hits",
                "page",
                "sort",
                "minPrice",
                "maxPrice",
                "orFlag",
                "genreInformationFlag",
            };
        }
        #endregion
    }
}
