﻿namespace RakutenWebService
{
    /// <summary>APIのベースURLを保持する定数クラス</summary>
    internal static class BaseUrlConst
    {
        /// <summary>楽天市場商品検索API</summary>
        public const string ICHIBA_ITEM_SEARCH = @"https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222";
        /// <summary>楽天ジャンル検索API</summary>
        public const string ICHIBA_GENRE = @"https://app.rakuten.co.jp/services/api/IchibaGenre/Search/20140222";
        /// <summary>楽天タグ検索API</summary>
        public const string ICHIBA_TAG = @"https://app.rakuten.co.jp/services/api/IchibaTag/Search/20140222";
        /// <summary>楽天商品ランキングAPI</summary>
        public const string ICHIBA_ITEM_RANKING = @"https://app.rakuten.co.jp/services/api/IchibaItem/Ranking/20120927";
        /// <summary>商品価格ナビ製品検索API</summary>
        public const string PRODUCT_SEARCH = @"https://app.rakuten.co.jp/services/api/Product/Search/20140305";
    }
}
