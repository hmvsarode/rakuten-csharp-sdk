﻿namespace RakutenWebService
{
    /// <summary>レスポンスフォーマット</summary>
    public enum ResponseFormat
    {
        /// <summary>XML形式</summary>
        XML,
        /// <summary>JSON形式</summary>
        JSON,
    }
}
