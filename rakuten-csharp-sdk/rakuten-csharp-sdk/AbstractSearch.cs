﻿using System.Collections.Generic;
using System.Net.Http;

namespace RakutenWebService
{
    /// <summary>各種検索の抽象クラス</summary>
    public abstract class AbstractSearch : ISearchable
    {
        /// <summary>アプリケーションID</summary>
        public string ApplicationId { get; protected set; }
        /// <summary>API毎のベースURL</summary>
        public string BaseUrl { get; protected set; }

        /// <summary>コンストラクタ</summary>
        /// <param name="applicationId">アプリケーションID</param>
        internal AbstractSearch(string applicationId)
        {
            this.ApplicationId = applicationId;
        }

        /// <summary>与えられた条件で検索を行う</summary>
        /// <param name="parameters">リクエストパラメータ</param>
        /// <exception cref="HttpException">HTTPエラー</exception>
        /// <returns>レスポンス</returns>
        public string Search(IDictionary<string, string> parameters)
        {
            using (var client = new HttpClient())
            {
                var request = new GetRequest(BaseUrl, ApplicationId, removeInvalidParameters(parameters));
                var response = client.GetAsync(request.Url()).Result;
                if (!response.IsSuccessStatusCode) throw new HttpException(response.StatusCode);
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        /// <summary>API毎に無効なパラメータを除く</summary>
        /// <param name="parameters">リクエストパラメータ</param>
        /// <returns>有効なリクエストパラメータ</returns>
        protected abstract IDictionary<string, string> removeInvalidParameters(IDictionary<string, string> parameters);
    }
}
